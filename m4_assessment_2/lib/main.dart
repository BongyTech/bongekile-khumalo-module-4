import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const SplashScreen(),
    );
  }
}

class SplashScreen extends StatelessWidget{
  const SplashScreen({Key? key}): super(key: key);
@override
Widget build(BuildContext context){
return AnimatedSplashScreen(
  splash:Column(
    children: [
      Image.asset('images/Logo.png'),
      const Text('My app', style: TextStyle(fontSize: 40,fontWeight: FontWeight.bold),)
    ],
), 
  nextScreen: const MyHomePage(title: 'Module4 Assesment 2',));
}
}
class MyHomePage extends StatelessWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;
  
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    throw UnimplementedError();
  }

}

